<?php
// Vérification si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupération des données du formulaire
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $email = $_POST['email'];

    // Connexion à la base de données
    $servername = "mysql";
    $username = "root";
    $password = "root";
    $dbname = "test_db";
    $port = 3306;

    $conn = new mysqli($servername, $username, $password, $dbname, $port);

    // Vérification de la connexion
    if ($conn->connect_error) {
        die("Connexion échouée : " . $conn->connect_error);
    }
    
    // Préparation et exécution de la requête d'insertion
    $sql = "INSERT INTO test (nom, prenom, email) VALUES ('$nom', '$prenom', '$email')";
    if ($conn->query($sql) === TRUE) {
        // Enregistrement réussi, envoi d'un e-mail à l'utilisateur
        $to = $email;
        $subject = "Confirmation d'enregistrement";
        $message = "Cher $prenom, votre enregistrement a été effectué avec succès.";
        $headers = "From: michel";

        // Envoi de l'e-mail
        if (mail($to, $subject, $message, $headers)) {
            echo "Enregistrement réussi. Un e-mail de confirmation a été envoyé à $email.";
        } else {
            echo "<h1>Enregistrement réussi, mais l'envoi de l'e-mail à $email a échoué. </h1>";
        }
    } else {
        echo "<h1>Erreur lors de l'enregistrement : " . $conn->error . "</h1>";
    }
    // Fermeture de la connexion
    $conn->close();
}

?>